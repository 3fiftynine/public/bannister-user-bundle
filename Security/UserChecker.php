<?php

declare(strict_types=1);

namespace Bannister\UserBundle\Security;

use Bannister\UserBundle\Entity\BaseUser;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserChecker
 * @package Bannister\UserBundle\Security
 */
class UserChecker implements UserCheckerInterface
{
    /**
     * @param UserInterface $user
     *
     * @return void
     */
    public function checkPreAuth(UserInterface $user): void
    {
        if ($user instanceof BaseUser === false) {
            return;
        }

        if ($user->isEnabled() === false) {
            throw new DisabledException();
        }

        if ($user->isDeleted() === true) {
            throw new DisabledException();
        }
    }

    /**
     * @param UserInterface $user
     *
     * @return void
     */
    public function checkPostAuth(UserInterface $user): void
    {
        // No PostAuth actions necessary.
    }
}
