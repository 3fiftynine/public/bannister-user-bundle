<?php

declare(strict_types=1);

namespace Bannister\UserBundle\EventListener;

use Bannister\UserBundle\Entity\BaseUser;
use DateTime;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Exception;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * This listener listens for changes to the User entity.
 * On persist or change, it takes the plain password field and encodes it.
 *
 * Class DoctrineUserListener
 * @package Bannister\UserBundle
 */
class DoctrineUserListener
{
    /**
     * DoctrineUserListener constructor.
     *
     * @param UserPasswordHasherInterface $passwordHasher
     */
    public function __construct(private readonly UserPasswordHasherInterface $passwordHasher)
    {
    }

    /**
     * @param BaseUser           $user
     * @param LifecycleEventArgs $event
     *
     * @throws Exception
     */
    public function prePersist(BaseUser $user, LifecycleEventArgs $event): void
    {
        $this->hashPassword($event->getObject());
        $user->setDateModified(new DateTime());
    }

    /**
     * @param BaseUser $user
     */
    public function hashPassword(BaseUser $user): void
    {
        $plainPassword = trim($user->getPlainPassword());
        $user->setPlainPassword($plainPassword);
        if (strlen($plainPassword) > 0) {
            $encodedPassword = $this->passwordHasher->hashPassword($user, $plainPassword);
            $user->setPassword($encodedPassword);
            $user->eraseCredentials();
        }
    }

    /**
     * @param BaseUser           $user
     * @param LifecycleEventArgs $event
     */
    public function preUpdate(BaseUser $user, LifecycleEventArgs $event): void
    {
        $this->hashPassword($event->getObject());
    }
}
