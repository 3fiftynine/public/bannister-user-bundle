Bannister User Bundle
=====================

About
-----
#### Version
`2.0.0`

#### Support Status
The *Bannister User Bundle* is no longer actively developed, and is simply being maintained for several in-house projects until it becomes obsolete.

#### Description
The *Bannister User Bundle* is a user bundle for Symfony-based applications.

With minimal effort on your part, it offers a simple implementation of user registration, activation, login, and password resetting.
However, the bundle is highly customizable, making it an easy and perfect fit for all your user-related needs.

#### Technical Requirements
The *Bannister User Bundle* requires:
- PHP 7.2+ or 8.0+
- Symfony 4.3+
- A working Swiftmailer configuration

Installation
------------
#### Step 1: Composer
Start off by opening your command-line and run the following command inside your project:

```console
composer require bannister/user-bundle:^1.1.0
```

#### Step 2: Enable the bundle
If you're using Symfony Flex, this step happens automatically on installing the bundle.
If you're not, however, add the following to your `config/bundles.php` configuration.

```php
<?php

// config/bundles.php

return [
    // ...
    Bannister\UserBundle\BannisterUserBundle::class => ['all' => true],
];
```

#### Step 3: Routing
You can then import the bundle's routing by adding the following configuration to your `config/routes.yaml` file.

```yaml
# config/routes.yaml

bannister_user:
    resource: "@BannisterUserBundle/Resources/config/routes.yaml"
```

#### Step 4: Security configuration
To properly function, the *Bannister User Bundle* needs you to tinker with your application's security a little.
For password encryption, you will need to set an encoder, and for the login/logout process, you will need a user provider.

Achieving this is simple, just make sure the configuration shown below is included in your own `config/packages/security.yaml`.
The `access_control` at the bottom is optional, but allows anonymous access to the *Bannister User Bundle*'s pages and enforces authentication for the rest of the application.

```yaml
# config/packages/security.yaml

security:
    encoders:
        App\Entity\User: auto
    providers:
        bannisterUserBundle:
            entity: { class: App\Entity\User, property: email }
    firewalls:
        main:
            anonymous: true
            form_login:
                provider: bannisterUserBundle
                csrf_token_generator: security.csrf.token_manager
                login_path: bannister_user_login
                check_path: bannister_user_login
                default_target_path: index
                username_parameter: _email
            logout:
                path: /logout
                target: /login
            user_checker: bannisterUserChecker


    access_control:
        - { path: ^/login$,                     role: [IS_AUTHENTICATED_ANONYMOUSLY] }
        - { path: ^/password-reset/request$,    role: [IS_AUTHENTICATED_ANONYMOUSLY] }
        - { path: ^/password-reset/reset/*,     role: [IS_AUTHENTICATED_ANONYMOUSLY] }
        - { path: ^/register$,                  role: [IS_AUTHENTICATED_ANONYMOUSLY] }
        - { path: ^/activate-user/activate/*,   role: [IS_AUTHENTICATED_ANONYMOUSLY] }
        - { path: ^/,                           role: [IS_AUTHENTICATED_FULLY] }
```

#### Step 5: Base templates
The *Bannister User Bundle* Twig templates make use of your base Twig templates, `templates/base.html.twig` and `templates/baseEmail.html.twig`.

Symfony automatically sets up any new projects with the first, but you have to add the latter yourself.
Once both templates exist, make sure you define a `{% block bannisterUserBundle %}{% endblock %}` block to allow the bundle to render its templates inside. 

#### Step 6: User entity
Proceed with the installation process by adding your own `User` entity and extend it from the bundle's `BaseUser` class as shown below.
Don't forget to create the table in your database!

```php
<?php

// Entity/User.php

namespace App\Entity;

use Bannister\UserBundle\Entity\BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    // TODO: Add your own logic here.
}
```

#### Step 7: Configuration
Finish the setup of the bundle by creating a configuration file for this bundle in `config/packages/bannister_user.yaml` and copying the default configuration from [the configuration reference](Resources/docs/configuration-reference.md) into it.

Documentation
-------------
For further documentation or help in setting up your user management with the *Bannister User Bundle*, please see [this bundle's documentation](Resources/docs/index.md).

For more information about the `packages/config/security.yaml` configuration, please refer to the Symfony framework documentation.

Authors
-------
|    Name    |      Function      |      Contact      |
|------------|--------------------|-------------------|
| Tom Peters | Back-end Developer | Tom@3Fiftynine.nl |