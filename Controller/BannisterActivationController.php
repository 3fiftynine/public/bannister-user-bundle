<?php

declare(strict_types=1);

namespace Bannister\UserBundle\Controller;

use Bannister\UserBundle\Entity\BaseUser;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * This controller manages user activation.
 *
 * Class BannisterActivationController
 * @package Bannister\UserBundle\Controller
 */
class BannisterActivationController extends AbstractController
{
    /**
     * @param EntityManagerInterface   $entityManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param Swift_Mailer             $mailer
     * @param TokenGeneratorInterface  $tokenGenerator
     * @param TokenStorageInterface    $tokenStorage
     * @param TranslatorInterface      $translator
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly Swift_Mailer $mailer,
        private readonly TokenGeneratorInterface $tokenGenerator,
        private readonly TokenStorageInterface $tokenStorage,
        private readonly TranslatorInterface $translator
    ) {
    }

    /**
     * Handles the sending of the user activation e-mail.
     *
     * @param BaseUser $user
     * @param array    $templateData
     * @param array    $routeData
     *
     * @return Response
     * @throws Exception
     */
    public function bannisterSendUserActivationEmail(BaseUser $user, array $templateData = [], array $routeData = []): Response
    {
        /*
         * Configuration parameters.
         */
        $fromAddress = $this->getParameter('bannister_user')['email']['from_address'];
        $fromName = $this->getParameter('bannister_user')['email']['from_name'];
        $route = $this->getParameter('bannister_user')['activation']['route'];
        $subject = $this->getParameter('bannister_user')['activation']['email_subject'];
        $template = $this->getParameter('bannister_user')['activation']['email_template'];
        $translationDomain = $this->getParameter('bannister_user')['translation_domain'];

        /*
         * Checks whether the activation request is still valid.
         */
        $noPendingPasswordRequest = $user->hasDatePasswordRequestExpired();
        if ($noPendingPasswordRequest === true) {
            /*
             * If this is a new activation request, generate a new password token.
             */
            if ($user->getPasswordToken() === false) {
                $passwordToken = $this->tokenGenerator->generateToken();
                $user->setPasswordToken($passwordToken);
            }

            $user->setDatePasswordRequested(new DateTime());
            $this->entityManager->flush();

            /*
             * Generate the activation URL and include it in the template data.
             */
            $routeData['passwordToken'] = $user->getPasswordToken();
            $activationUrl = $this->generateUrl($route, $routeData, UrlGeneratorInterface::ABSOLUTE_URL);
            $templateData['activationUrl'] = $activationUrl;

            /*
             * Render and send the user activation e-mail.
             */
            $message = new Swift_Message($this->translator->trans($subject, domain: $translationDomain));
            $message
                ->setFrom($fromAddress, $fromName)
                ->setTo($user->getEmail())
                ->setBody($this->renderView($template, $templateData), 'text/html');
            $this->mailer->send($message);
        }

        return new Response('Activation e-mail sent.');
    }

    /**
     * Handles user activation.
     * GET: Renders the user activation page.
     * POST: Activates the user.
     *
     * @param Request $request       The user activation request as forwarded from the implemented controller.
     * @param string  $passwordToken The unique password token identifying the user.
     * @param array   $templateData  Additional data required by the user activation page template.
     * @param array   $formData      Additional data required by the user activation form template.
     * @param array   $routeData     Additional parameters and/or data required by the user activation route.
     *
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function bannisterActivateUser(Request $request, string $passwordToken, array $templateData = [], array $formData = [], array $routeData = []): RedirectResponse|Response
    {
        /*
         * Configuration parameters.
         */
        $authenticationRequired = $this->getParameter('bannister_user')['activation']['authentication_on_success'];
        $firewallName = $this->getParameter('bannister_user')['firewall'];
        $formClass = $this->getParameter('bannister_user')['activation']['form'];
        $route = $this->getParameter('bannister_user')['activation']['route'];
        $successRoute = $this->getParameter('bannister_user')['activation']['on_success_route'];
        $template = $this->getParameter('bannister_user')['activation']['page_template'];
        $userClass = $this->getParameter('bannister_user')['user_entity_class'];

        /*
         * Determine whether the activation request is valid.
         */
        /** @var BaseUser $user */
        $user = $this->entityManager->getRepository($userClass)->findOneBy(['passwordToken' => $passwordToken]);
        if ($user === null) {
            throw new Exception(sprintf('No user found for password token: %s.', $passwordToken));
        } elseif ($user->isEnabled()) {
            throw new Exception(sprintf('User (%s) is already active.', $user->getEmail()));
        } elseif ($user->hasDatePasswordRequestExpired()) {
            throw new Exception(sprintf('User (%s) activation request has expired.', $user->getEmail()));
        }

        /*
         * Generate the form and process the request data.
         */
        $routeData['passwordToken'] = $passwordToken;
        $formData['action'] = $this->generateUrl($route, $routeData);
        $form = $this->createForm($formClass, $user, $formData)->handleRequest($request);
        if ($form->isSubmitted() === true && $form->isValid() === true) {
            /*
             * If the form submission was successful, update the user to activate it.
             */
            $rawPlainPassword = $form->getName() === '' ? $request->get('plainPassword') : $request->get($form->getName())['plainPassword'];
            $plainPassword = is_array($rawPlainPassword) === true ? $rawPlainPassword['first'] : $rawPlainPassword;
            $user
                ->setIsEnabled(true)
                ->setPlainPassword($plainPassword)
                ->setPasswordToken()
                ->setDatePasswordRequested();
            $this->entityManager->flush();

            /*
             * If authentication has been enabled, the user will be logged in, and redirected afterwards.
             */
            if ($authenticationRequired === true) {
                $loginToken = new UsernamePasswordToken($user, $user->getPassword(), $firewallName, $user->getRoles());
                $this->tokenStorage->setToken($loginToken);
                $request->getSession()->set('_security_main', serialize($loginToken));
                $event = new InteractiveLoginEvent($request, $loginToken);
                $this->eventDispatcher->dispatch($event, 'security.interactive_login');
            }

            return $this->redirectToRoute($successRoute);
        }

        /*
         * Finalize the template data and subsequently render the user activation template.
         */
        $templateData['form'] = $form->createView();
        $templateData['error'] = $form->getErrors(true);
        $templateData['user'] = $user;

        return $this->render($template, $templateData);
    }
}
