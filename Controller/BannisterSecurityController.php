<?php

declare(strict_types=1);

namespace Bannister\UserBundle\Controller;

use Bannister\UserBundle\Entity\BaseUser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * This controller manages user authentication, login, and registration.
 *
 * Class BannisterSecurityController
 * @package Bannister\UserBundle
 */
class BannisterSecurityController extends AbstractController
{
    /**
     * Handles user login and authentication.
     * GET: Renders the login form.
     * POST: Authenticates the user's credentials and allows them to log in.
     *
     * @param AuthenticationUtils $authenticationUtils
     * @param array               $templateData Additional data required by the login page template.
     * @param array               $formData     Additional data required by the login form.
     *
     * @return Response
     */
    public function bannisterLogin(AuthenticationUtils $authenticationUtils, array $templateData = [], array $formData = []): Response
    {
        /*
         * Configuration parameters.
         */
        $formClass = $this->getParameter('bannister_user')['login']['form'];
        $route = $this->getParameter('bannister_user')['login']['route'];
        $template = $this->getParameter('bannister_user')['login']['page_template'];

        /*
         * Form generation and template preparation.
         */
        $formData['action'] = $this->generateUrl($route);
        $form = $this->createForm($formClass, options: $formData);
        $templateData['form'] = $form->createView();
        $templateData['error'] = $authenticationUtils->getLastAuthenticationError();
        $templateData['lastUsername'] = $authenticationUtils->getLastUsername();

        return $this->render($template, $templateData);
    }

    /**
     * Handles user registration.
     * GET: Renders the registration form.
     * POST: Registers the new user.
     *
     * @param EntityManagerInterface $entityManager
     * @param Request                $request
     * @param BaseUser|null          $user
     * @param array                  $templateData
     * @param array                  $formData
     * @param array                  $emailTemplateData
     * @param array                  $activationRouteData
     *
     * @return RedirectResponse|Response
     */
    public function bannisterRegistration(
        EntityManagerInterface $entityManager,
        Request $request,
        BaseUser $user = null,
        array $templateData = [],
        array $formData = [],
        array $emailTemplateData = [],
        array $activationRouteData = []
    ): RedirectResponse|Response {
        /*
         * Configuration parameters.
         */
        $formClass = $this->getParameter('bannister_user')['registration']['form'];
        $route = $this->getParameter('bannister_user')['registration']['route'];
        $successRoute = $this->getParameter('bannister_user')['registration']['on_success_route'];
        $template = $this->getParameter('bannister_user')['registration']['page_template'];
        $userClass = $this->getParameter('bannister_user')['user_entity_class'];

        /*
         * Generate the form and process the request data.
         */
        $user = $user ?: new $userClass();
        $formData['action'] = $this->generateUrl($route);
        $form = $this->createForm($formClass, $user, $formData)->handleRequest($request);
        if ($form->isSubmitted() === true && $form->isValid() === true) {
            /*
             * If the form submission was successful, create the user and send an activation e-mail.
             */
            $email = $request->get('email');
            $username = $request->get('username');
            $user
                ->setEmail($email)
                ->setUsername($username);

            if (empty($user->getRoles()) === true) {
                $user->addRole('ROLE_USER');
            }

            $entityManager->persist($user);
            $entityManager->flush();

            $this->forward('Bannister\UserBundle\Controller\BannisterActivationController::bannisterSendUserActivationEmail', [
                'user' => $user,
                'templateData' => $emailTemplateData,
                'routeData' => $activationRouteData
            ]);

            return $this->redirectToRoute($successRoute);
        }

        /*
         * Finalize the template data and subsequently render the user activation template.
         */
        $templateData['form'] = $form->createView();
        $templateData['error'] = $form->getErrors(true);
        $templateData['user'] = $user;

        return $this->render($template, $templateData);
    }
}
