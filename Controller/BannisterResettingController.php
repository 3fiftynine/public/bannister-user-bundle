<?php

declare(strict_types=1);

namespace Bannister\UserBundle\Controller;

use Bannister\UserBundle\Entity\BaseUser;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * This controller manages requests for new passwords by a user.
 *
 * Class BannisterResettingController
 * @package Bannister\UserBundle
 */
class BannisterResettingController extends AbstractController
{
    /**
     * @param EntityManagerInterface   $entityManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param Swift_Mailer             $mailer
     * @param TokenGeneratorInterface  $tokenGenerator
     * @param TokenStorageInterface    $tokenStorage
     * @param TranslatorInterface      $translator
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly Swift_Mailer $mailer,
        private readonly TokenGeneratorInterface $tokenGenerator,
        private readonly TokenStorageInterface $tokenStorage,
        private readonly TranslatorInterface $translator
    ) {
    }

    /**
     * Handles the user request for a password reset.
     * GET: Renders the password request page.
     * POST: Sends the password reset e-mail and redirects the user.
     *
     * @param Request $request      The password request as forwarded from the implemented controller.
     * @param array   $templateData Additional data required by the password request page template.
     * @param array   $formData     Additional data required by the password request form template.
     * @param array   $emailData    Additional data required by the e-mail template.
     * @param array   $routeData    Additional parameters and/or data required by the password reset route.
     *
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function bannisterRequestPassword(Request $request, array $templateData = [], array $formData = [], array $emailData = [], array $routeData = []): RedirectResponse|Response
    {
        /*
         * Configuration parameters.
         */
        $authorizationRequired = $this->getParameter('bannister_user')['resetting']['authentication_on_success_request'];
        $firewallName = $this->getParameter('bannister_user')['firewall'];
        $formClass = $this->getParameter('bannister_user')['resetting']['form_request'];
        $template = $this->getParameter('bannister_user')['resetting']['page_template_request'];
        $route = $this->getParameter('bannister_user')['resetting']['route_request'];
        $successRoute = $this->getParameter('bannister_user')['resetting']['on_success_route_request'];
        $userClass = $this->getParameter('bannister_user')['user_entity_class'];

        $formData['action'] = $this->generateUrl($route);
        $form = $this->createForm($formClass, options: $formData)->handleRequest($request);
        if ($form->isSubmitted() === true && $form->isValid() === true) {
            /*
             * If the form submission was successful, send the user a password reset e-mail.
             */
            $email = $form->getName() === '' ? $request->get('email') : $request->get($form->getName())['email'];
            /** @var BaseUser $user */
            $user = $this->entityManager->getRepository($userClass)->findOneBy(['email' => $email]);
            if ($user !== null) {
                $this->bannisterSendPasswordRequestEmail($user, $emailData, $routeData);

                /*
                 * If authentication has been enabled, the user will be logged in, and redirected afterwards.
                 */
                if ($authorizationRequired === true) {
                    $loginToken = new UsernamePasswordToken($user, $user->getPassword(), $firewallName, $user->getRoles());
                    $this->tokenStorage->setToken($loginToken);
                    $request->getSession()->set('_security_main', serialize($loginToken));
                    $event = new InteractiveLoginEvent($request, $loginToken);
                    $this->eventDispatcher->dispatch($event, 'security.interactive_login');
                }
            }

            return $this->redirectToRoute($successRoute);
        }

        /*
         * Finalize the template data and subsequently render the password request template.
         */
        $templateData['form'] = $form->createView();
        $templateData['error'] = $form->getErrors(true);

        return $this->render($template, $templateData);
    }

    /**
     * Handles the sending of the password reset request e-mail to the user it was requested by.
     *
     * @param BaseUser $user         The user requesting the password reset.
     * @param array    $templateData Additional data required by the e-mail template.
     * @param array    $routeData    Additional parameters and/or data required by the password reset route.
     *
     * @throws Exception
     */
    public function bannisterSendPasswordRequestEmail(BaseUser $user, array $templateData = [], array $routeData = [])
    {
        /*
         * Configuration parameters.
         */
        $fromAddress = $this->getParameter('bannister_user')['email']['from_address'];
        $fromName = $this->getParameter('bannister_user')['email']['from_name'];
        $route = $this->getParameter('bannister_user')['resetting']['route_reset'];
        $subject = $this->getParameter('bannister_user')['resetting']['email_subject'];
        $template = $this->getParameter('bannister_user')['resetting']['email_template'];
        $translationDomain = $this->getParameter('bannister_user')['translation_domain'];

        /*
         * Checks whether the password request is still valid.
         */
        $noPendingPasswordRequest = $user->hasDatePasswordRequestExpired();
        if ($noPendingPasswordRequest === true) {
            /*
             * If this is a new activation request, generate a new password token.
             */
            if ($user->getPasswordToken() === null) {
                $passwordToken = $this->tokenGenerator->generateToken();
                $user->setPasswordToken($passwordToken);
            }

            $user->setDatePasswordRequested(new DateTime());
            $this->entityManager->flush();

            /*
             * Generate the reset password URL and include it in the template data.
             */
            $routeData['passwordToken'] = $user->getPasswordToken();
            $resetPasswordUrl = $this->generateUrl($route, $routeData, UrlGeneratorInterface::ABSOLUTE_URL);
            $templateData['resetUrl'] = $resetPasswordUrl;

            /*
             * Render and send the user activation e-mail.
             */
            $message = new Swift_Message($this->translator->trans($subject, domain: $translationDomain));
            $message
                ->setFrom($fromAddress, $fromName)
                ->setTo($user->getEmail())
                ->setBody($this->renderView($template, $templateData), 'text/html');
            $this->mailer->send($message);
        }
    }

    /**
     * Handles the actual user password reset.
     * GET: Renders the password reset page.
     * POST: Updates the user's password and redirects the user.
     *
     * @param Request $request       The password reset request as forwarded from the implemented controller.
     * @param string  $passwordToken The unique password token identifying the user.
     * @param array   $templateData  Additional data required by the password reset page template.
     * @param array   $formData      Additional data required by the password reset form template.
     * @param array   $routeData     Additional parameters and/or data required by the password reset route.
     *
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function bannisterResetPassword(Request $request, string $passwordToken, array $templateData = [], array $formData = [], array $routeData = []): RedirectResponse|Response
    {
        /*
         * Configuration parameters.
         */
        $authorizationRequired = $this->getParameter('bannister_user')['resetting']['authentication_on_success_reset'];
        $firewallName = $this->getParameter('bannister_user')['firewall'];
        $formClass = $this->getParameter('bannister_user')['resetting']['form_reset'];
        $template = $this->getParameter('bannister_user')['resetting']['page_template_reset'];
        $route = $this->getParameter('bannister_user')['resetting']['route_reset'];
        $successRoute = $this->getParameter('bannister_user')['resetting']['on_success_route_reset'];
        $userClass = $this->getParameter('bannister_user')['user_entity_class'];

        /*
         * Checks whether the password request is still valid.
         */
        /** @var BaseUser $user */
        $user = $this->entityManager->getRepository($userClass)->findOneBy(['passwordToken' => $passwordToken]);
        if (!$user || $user->hasDatePasswordRequestExpired()) {
            throw new Exception('Invalid password token provided.');
        }

        $routeData['passwordToken'] = $passwordToken;
        $formData['action'] = $this->generateUrl($route, $routeData);
        $form = $this->createForm($formClass, $user, $formData)->handleRequest($request);
        if ($form->isSubmitted() === true && $form->isValid() === true) {
            /*
             * If the form submission was successful, reset the user's email.
             */
            $rawPlainPassword = $form->getName() === '' ? $request->get('plainPassword') : $request->get($form->getName())['plainPassword'];
            $plainPassword = is_array($rawPlainPassword) === true ? $rawPlainPassword['first'] : $rawPlainPassword;
            $user
                ->setPlainPassword($plainPassword)
                ->setPasswordToken()
                ->setDatePasswordRequested();
            $this->entityManager->flush();

            /*
             * If authentication has been enabled, the user will be logged in, and redirected afterwards.
             */
            if ($authorizationRequired === true) {
                $loginToken = new UsernamePasswordToken($user, $user->getPassword(), $firewallName, $user->getRoles());
                $this->tokenStorage->setToken($loginToken);
                $request->getSession()->set('_security_main', serialize($loginToken));
                $event = new InteractiveLoginEvent($request, $loginToken);
                $this->eventDispatcher->dispatch($event, 'security.interactive_login');
            }

            return $this->redirectToRoute($successRoute);
        }

        /*
         * Finalize the template data and subsequently render the password request template.
         */
        $templateData['form'] = $form->createView();
        $templateData['error'] = $form->getErrors(true);
        $templateData['user'] = $user;

        return $this->render($template, $templateData);
    }
}
