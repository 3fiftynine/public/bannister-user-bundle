Password Resetting
=============
Introduction
------------
This chapter will discuss the standard functionality offered by the *Bannister User Bundle* for password resetting.

Summary
-------
Anyone can access the page for requesting a new password, but only activated users can actually reset their password.

They can do so by entering their e-mail address on the password request page, after which they are sent an e-mail containing the link to their personal password reset page.
Once there, they are requested to choose their new password, and after doing so successfully, are redirected back to the login page.

The *Bannister User Bundle* does not notify the user of invalid or non-existent credentials during the user activation and password reset flows out of security concerns.
Should you require such functionality for your application, you will need to extend the `BannisterResettingController` and add the logic there.

Route
-----
Anonymous users are allowed the approach the `bannister_user_password_request` route via `/password-reset/request`, where they are asked to enter their e-mail address.

The user then receives an e-mail containing the password reset link, generated from the `bannister_user_password_reset` route along with their generated password token.

Once they follow that link, they are directed to `/password-reset/reset/{passwordToken}`, where they can choose a new password.

Form
----
The password request page uses the `PasswordRequestType` form, consisting of just an `email` field and a `submit` button.

The password reset page uses the `PasswordResetType` form, another simple form with just a repeated `plainPassword` field and a `submit` button.

By default, the forms use the `bannisterUserBundle` translation domain for their form field labels.

Template
--------
Both the `password-reset/request.html.twig` and `password-reset/reset.html.twig` Twig templates are simple pages extending the `base.html.twig` template, and requiring a `form` Twig variable to properly render.
Both the request and reset forms are rendered inside `<div>` elements with the `bannister-user-bundle--password-request-form` and `bannister-user-bundle--password-reset-form` classes, respectively.

The `password-reset/email.html.twig` Twig template is a bare bones e-mail template extending the `baseEmail.html.twig` template, requiring only a `resetUrl` Twig variable to link the user to the password reset page.
The entirety of the e-mail is rendered within a `<div>` element with the `bannister-user-bundle--password-reset-email` class, inside which are two other `<div>` elements containing the e-mail text and link with the `bannister-user-bundle--password-reset-email--text` and `bannister-user-bundle--password-reset-email--link` classes, respectively.

The entirety of the templates are rendered within a `{% block bannisterUserBundle %}` block, making this block mandatory in the extended base templates.

Controller
----------
The `BannisterResettingController`'s `bannisterRequestPassword` function is called via the `bannister_user_password_request` route, which accepts both `GET` and `POST` in order to render the password request page and send the resetting e-mail, respectively. 

On a `GET` request, it generates the password request form and then returns the rendered Twig template.

However, on a `POST` request, the same form is generated and then handles the request data using Symfony's native form handling.
The user is then retrieved by their e-mail address and is handed over to the `BannisterResettingController`'s `bannisterSendPasswordRequestEmail` function.

The `BannisterResettingController`'s `bannisterSendPasswordRequestEmail` function checks whether the user has a pending activation request, and if so, returns a valid response without sending an e-mail.
In the absence of an active activation request, the function starts one by setting the `datePasswordRequested` and `passwordToken` of the user and finishes by sending an e-mail.
Subsequently, the user is redirected to the login page.

The `BannisterResettingController`'s `bannisterResetPassword` function is called via the `bannister_user_password_reset` route, which accepts both `GET` and `POST` in order to render the password reset page and reset the user's password, respectively. 

On a `GET` request, it generates the password reset form using a user object, retrieved via its `passwordToken` property, and then returns the rendered Twig template.

However, on a `POST` request, the same form is generated and then handles the request data using Symfony's native form handling.
The retrieved user's password is then reset by emptying the `datePasswordRequested` and `passwordToken` properties, and setting the password via `plainPassword`. 
Subsequently, the user is redirected to the login page.

It is possible to customize the redirect on password request or reset via the *Bannister User Bundle* configuration, with the option of enabling authentication to allow a redirect to somewhere inside your application.

Table of Contents
-----------------
0. [Bundle installation](../../README.md)
0. [Bannister User Registration](bannister-user-registration.md)
0. [Bannister User Activation](bannister-user-activation.md)
0. [Bannister Login](bannister-login.md)
0. **[Bannister Password Resetting](bannister-password-reset.md)**
0. [Customizing Bannister Base User](customizing-bannister-base-user.md)
0. [Customizing Bannister Routes](customizing-bannister-routes.md)
0. [Customizing Bannister Forms](customizing-bannister-forms.md)
0. [Customizing Bannister Templates](customizing-bannister-templates.md)
0. [Customizing Bannister Controllers](customizing-bannister-controllers.md)
0. [Configuration reference](configuration-reference.md)