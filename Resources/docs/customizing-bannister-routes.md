Route Customization
=============
Introduction
------------
This section will discuss the basics of how to customize the *Bannister User Bundle*'s routing to your needs.

How-to
------
Changing the default routing is quite easy, simply open the `bannister_user.yaml` configuration file and replace one of the configured routes with your own.
From that point on, the bundle will handle things.

Example
-------
Say you defined your password resetting function and attached it to the route `myVeryOwnPasswordReset`.

You could then simply set the value of the password resetting route in `bannister_user.yaml` to that value, as such:

```yaml
# config/packages/bannister_user.yaml

bannister_user:
    resetting:
        route_reset: myVeryOwnPasswordReset
```

Now, when the password reset link for the resetting e-mail is generated, it will use your route rather than the default one.

Table of Contents
-----------------
0. [Bundle installation](../../README.md)
0. [Bannister User Registration](bannister-user-registration.md)
0. [Bannister User Activation](bannister-user-activation.md)
0. [Bannister Login](bannister-login.md)
0. [Bannister Password Resetting](bannister-password-reset.md)
0. [Customizing Bannister Base User](customizing-bannister-base-user.md)
0. **[Customizing Bannister Routes](customizing-bannister-routes.md)**
0. [Customizing Bannister Forms](customizing-bannister-forms.md)
0. [Customizing Bannister Templates](customizing-bannister-templates.md)
0. [Customizing Bannister Controllers](customizing-bannister-controllers.md)
0. [Configuration reference](configuration-reference.md)