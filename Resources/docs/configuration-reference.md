Configuration Reference
=============
Description
-------
#### Security firewall (`firewall`)
This value should refer to the name of your primary firewall, as defined in your `security.yaml`, and is used for authenticating a user after a successful activation or password reset.

Defaults to `main`.

---

#### Translation domain (`translation_domain`)
The translation domain is used for translating the e-mail subjects for user activation and password resetting.
If you have defined your own subjects outside of the `bannisterUserBundle` namespace, adjust this value to your preferred translation domain.

Defaults to `bannisterUserBundle`.

---

#### User entity (`user_entity_class`)
The user class (with namespace) which extends the *Bannister User Bundle*'s `BaseUser` class.
This class is used for creating and updating the user in all parts of the bundle.

Defaults to `App\Entity\User`.

---

#### E-mail (`email`)
`from_address`: The e-mail address shown as the sender of activation and password reset e-mails (default: `Support@Bannister.com`). 

`from_name`: The name of the e-mail sender of activation and password reset e-mails (default: `Bannister User Support`).

---

#### User registration (`registration`)
`route`: The route name used for generating the registration page and handling its form output (default: `bannister_user_registration`).

`form`: The form class (with namespace) used for rendering the user registration form (default: `Bannister\UserBundle\Form\RegistrationType`).

`page_template`: The Twig template in which the registration form is rendered (default: `@BannisterUser/security/registration.html.twig`).

`on_success_route`: The route to which the user is anonymously redirected after successful registration, pending their activation (default: `bannister_user_login`).

---

#### User activation (`activation`)
`route`: The route name used for generating the user activation page and handling its form output (default: `bannister_user_activate_user`).

`form`: The form class (with namespace) used for rendering the user activation form (default: `Bannister\UserBundle\Form\ActivationType`).

`email_subject`: The translation key for the subject line of the user activation e-mail (default: `activation.subject`).

`email_template`: The Twig template which is rendered for the user activation e-mail (default: `@BannisterUser/activation/email.html.twig`).

`page_template`: The Twig template in which the user activation form is rendered (default: `@BannisterUser/activation/activation.html.twig`).

`authentication_on_success`: A boolean value to indicate whether the `on_success_route` for user activation requires user authentication (default: `false`).

`on_success_route`: The route to which the user is redirected after successful activation (default: `bannister_user_login`).

---

#### User authentication (`login`)
`route`: The route name used for generating the login page and handling its form output (default: `bannister_user_login`).

`form`: The form class (with namespace) used for rendering the login form (default: `Bannister\UserBundle\Form\LoginType`).

`page_template`: The Twig template in which the login form is rendered (default: `@BannisterUser/security/login.html.twig`).

---

#### Password reset (`resetting`)
`route_request`: The route name used for generating the password request page and handling its form output (default: `bannister_user_password_request`).

`route_reset`: The route name used for generating the password reset page and handling its form output (default: `bannister_user_password_reset`).

`form_request`: The form class (with namespace) used for rendering the password request form (default: `Bannister\UserBundle\Form\PasswordRequestType`).

`form_reset`: The form class (with namespace) used for rendering the password reset form (default: `Bannister\UserBundle\Form\PasswordResetType`).

`email_subject`: The translation key for the subject line of the password reset e-mail (default: `passwordReset.subject`).

`email_template`: The Twig template which is rendered for the password reset e-mail (default: `@BannisterUser/password-reset/email.html.twig`).

`page_template_request`: The Twig template in which the password request form is rendered (default: `@BannisterUser/password-reset/request.html.twig`).

`page_template_reset`: The Twig template in which the password reset form is rendered (default: `@BannisterUser/password-reset/reset.html.twig`).
 
`authentication_on_success_request`: A boolean value to indicate whether the `on_success_route` for password request requires user authentication (default: `false`).

`on_success_route_request`: The route to which the user is redirected after successfully requesting a new password (default: `bannister_user_login`).

`authentication_on_success_reset`: A boolean value to indicate whether the `on_success_route` for password reset requires user authentication (default: `false`).

`on_success_route_reset`: The route to which the user is redirected after successfully resetting their password (default: `bannister_user_login`).

Full configuration
-----------------
```yaml
# config/packages/bannister_user.yaml

bannister_user:
    firewall: main
    translation_domain: bannisterUserBundle
    user_entity_class: App\Entity\User
    email:
        from_address: Support@Bannister.com
        from_name: Bannister Support
    registration:
        route: bannister_user_registration
        form: Bannister\UserBundle\Form\RegistrationType
        page_template: "@BannisterUser/security/registration.html.twig"
        on_success_route: bannister_user_login
    activation:
        route: bannister_user_activate_user
        form: Bannister\UserBundle\Form\ActivationType
        email_subject: activation.subject
        email_template: "@BannisterUser/activation/email.html.twig"
        page_template: "@BannisterUser/activation/activation.html.twig"
        authentication_on_success: false
        on_success_route: bannister_user_login
    login:
        route: bannister_user_login
        form: Bannister\UserBundle\Form\LoginType
        page_template: "@BannisterUser/security/login.html.twig"
    resetting:
        route_request: bannister_user_password_request
        route_reset: bannister_user_password_reset
        form_request: Bannister\UserBundle\Form\PasswordRequestType
        form_reset: Bannister\UserBundle\Form\PasswordResetType
        email_subject: passwordReset.subject
        email_template: "@BannisterUser/password-reset/email.html.twig"
        page_template_request: "@BannisterUser/password-reset/request.html.twig"
        page_template_reset: "@BannisterUser/password-reset/reset.html.twig"
        authentication_on_success_request: false
        on_success_route_request: bannister_user_login
        authentication_on_success_reset: false
        on_success_route_reset: bannister_user_login
```

Chapters
-----------------
0. [Bundle installation](../../README.md)
0. [Bannister User Registration](bannister-user-registration.md)
0. [Bannister User Activation](bannister-user-activation.md)
0. [Bannister Login](bannister-login.md)
0. [Bannister Password Resetting](bannister-password-reset.md)
0. [Customizing Bannister Base User](customizing-bannister-base-user.md)
0. [Customizing Bannister Routes](customizing-bannister-routes.md)
0. [Customizing Bannister Forms](customizing-bannister-forms.md)
0. [Customizing Bannister Templates](customizing-bannister-templates.md)
0. [Customizing Bannister Controllers](customizing-bannister-controllers.md)
0. **[Configuration reference](configuration-reference.md)**