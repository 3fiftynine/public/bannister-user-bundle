User Authentication & Login
=============
Introduction
------------
This chapter will discuss the standard functionality offered by the *Bannister User Bundle* for user authentication and login.

Summary
-------
After having activated a new user, they are now able to login to your application with their username or e-mail and password via the *Bannister User Bundle* login page.
Please note that the *Bannister User Bundle* utilizes a user's e-mail as their login username by default.

Route
-----
Anonymous users are allowed the approach the `bannister_user_login` route via `/login`.
The user will be able to fill in their login credentials here.

Form
----
The login page uses the `LoginType` form, a small form with `_email` and `_password` fields and a `submit` button.
By default, the form uses the `bannisterUserBundle` translation domain for its form field labels.

Template
--------
The `security/login.html.twig` Twig template is a minimalistic page extending the `base.html.twig` template, merely requiring the `form` Twig variable to properly render.
The form is rendered inside a `<div>` element with the `bannister-user-bundle--login-form` class.

What this template does different compared to other templates in this bundle, is render the form piece-by-piece in order to insert a hidden `_csrf_token` field, which is necessary for proper authentication of the user.

The entirety of the templates are rendered within a `{% block bannisterUserBundle %}` block, making this block mandatory in the extended base templates.

Controller
----------
The `BannisterSecurityController`'s `bannisterLogin` function is called via the `bannister_user_login` route, which accepts both `GET` and `POST` in order to render the login page and authenticate the user, respectively. 

On a `GET` request, it generates the login form and then returns the rendered Twig template.

However, on a `POST` request, Symfony should be configured to authenticate the user using the credentials provided.
On success, it will direct the authenticated user to a route of your choice, but should authentication fail, they will be returned to the login page with an appropriate error message visible.

Table of Contents
-----------------
0. [Bundle installation](../../README.md)
0. [Bannister User Registration](bannister-user-registration.md)
0. [Bannister User Activation](bannister-user-activation.md)
0. **[Bannister Login](bannister-login.md)**
0. [Bannister Password Resetting](bannister-password-reset.md)
0. [Customizing Bannister Base User](customizing-bannister-base-user.md)
0. [Customizing Bannister Routes](customizing-bannister-routes.md)
0. [Customizing Bannister Forms](customizing-bannister-forms.md)
0. [Customizing Bannister Templates](customizing-bannister-templates.md)
0. [Customizing Bannister Controllers](customizing-bannister-controllers.md)
0. [Configuration reference](configuration-reference.md)