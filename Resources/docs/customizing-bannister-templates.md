Twig Template Customization
=============
Introduction
------------
This section will discuss the basics of how to customize the *Bannister User Bundle*'s Twig templates to your needs.

Extension
---------
You can extend a *Bannister User Bundle* Twig template as you would any other Twig template.

Let us use the user activation template as an example and assume you wish to add your own text and change the `<div>` element's class.
The `activation.html.twig` file looks like this by default:

```twig
{# @BannisterUser/templates/activation/activation.html.twig #}
{% extends 'base.html.twig' %}

{% block bannisterUserBundle %}
  <div class="bannister-user-bundle--activation-form">
    {{ form(form) }}
  </div>
{% endblock %}
```

Next, simply create your own Twig template (e.g. `myActivation.html.twig`), extend the bundle's template, and make your changes in the `bannisterUserBundle` block.

```twig
{# templates/activation/myActivation.html.twig #}

{% extends '@BannisterUser/activation/activation.html.twig' %}

{% block bannisterUserBundle %}
  <div class="my-activation-form">
    {{ form(form) }}
  </div>
{% endblock %}
```

To make use of your changes, simply replace the page template used for the activation process in the `bannister_user.yaml` configuration file.

```yaml
# config/packages/bannister_user.yaml

bannister_user:
    activation:
        page_template: "activation/myActivation.html.twig"
```

Replacement
-----------
In order to replace one of this bundle's own Twig templates, create your own and add it to the bundle configuration.
Once you have done so, the bundle will use your template for that part of user management.

```yaml
# config/packages/bannister_user.yaml

bannister_user:
    registration:
        page_template: "@BannisterUser/security/registration.html.twig"
    activation:
        page_template: "@BannisterUser/activation/activation.html.twig"
    login:
        page_template: "@BannisterUser/security/login.html.twig"
    resetting:
        page_template_request: "@BannisterUser/password-reset/request.html.twig"
        page_template_reset: "@BannisterUser/password-reset/reset.html.twig"
```

Table of Contents
-----------------
0. [Bundle installation](../../README.md)
0. [Bannister User Registration](bannister-user-registration.md)
0. [Bannister User Activation](bannister-user-activation.md)
0. [Bannister Login](bannister-login.md)
0. [Bannister Password Resetting](bannister-password-reset.md)
0. [Customizing Bannister Base User](customizing-bannister-base-user.md)
0. [Customizing Bannister Routes](customizing-bannister-routes.md)
0. [Customizing Bannister Forms](customizing-bannister-forms.md)
0. **[Customizing Bannister Templates](customizing-bannister-templates.md)**
0. [Customizing Bannister Controllers](customizing-bannister-controllers.md)
0. [Configuration reference](configuration-reference.md)