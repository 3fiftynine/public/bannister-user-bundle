User Registration
=============
Introduction
------------
This chapter will discuss the standard functionality offered by the *Bannister User Bundle* for user registration.

Summary
-------
Users navigating to `/register` will be shown a simple registration form, consisting of a username and e-mail field.
After filling out the form and submitting it, the controller will validate the input and, if valid, create a new user and start the user activation process.

Route
-----
Anonymous users are allowed the approach the `bannister_user_registration` route via the `/register`, which subsequently activates the `bannisterRegistration` function in the `BannisterSecurityController` controller.

Form
----
The registration process uses the `RegistrationType` form, a simple form with `email` and `username` fields, and a `submit` button.
By default, the form uses the `bannisterUserBundle` translation domain for its form field labels.

Template
--------
The `security/registration.html.twig` Twig template is a minimalistic page extending the `base.html.twig` template, and requiring a `form` Twig variable to properly render.
The form is rendered inside a `<div>` element with the `bannister-user-bundle--registration-form` class.

The entirety of the template is rendered within a `{% block bannisterUserBundle %}` block, making this block mandatory in the extended base template.

Controller
----------
The `BannisterSecurityController`'s `bannisterRegistration` function accepts both `GET` and `POST` requests for the rendering and processing of the registration form, respectively.

On a `GET` request, it generates the registration form using a new user object and then returns the rendered Twig template.

However, on a `POST` request, the same form is generated and then handles the request data using Symfony's native form handling.
A new user record is subsequently persisted to the database using the provided username and e-mail.
By default, a `ROLE_USER` user role is also added to the user provided that they have no other role assigned to them.

At this point, the user has been persisted and is inactive, and is then forwarded into the user activation process via the `bannisterSendUserActivationEmail` function of the `BannisterActivationController` controller.

Table of Contents
-----------------
0. [Bundle installation](../../README.md)
0. **[Bannister User Registration](bannister-user-registration.md)**
0. [Bannister User Activation](bannister-user-activation.md)
0. [Bannister Login](bannister-login.md)
0. [Bannister Password Resetting](bannister-password-reset.md)
0. [Customizing Bannister Base User](customizing-bannister-base-user.md)
0. [Customizing Bannister Routes](customizing-bannister-routes.md)
0. [Customizing Bannister Forms](customizing-bannister-forms.md)
0. [Customizing Bannister Templates](customizing-bannister-templates.md)
0. [Customizing Bannister Controllers](customizing-bannister-controllers.md)
0. [Configuration reference](configuration-reference.md)